package main

import (
	"gitlab.com/commonground/don/validators/engine"

  "gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/validator/pkg/rulesets/verwerkingenlogging"
)

const (
	name        = "vwl-validator"
	description = "Tool to validate if an API conforms to the Verwerkingenlogging Standard"
)

var (
	version = "dev"
	commit  = "unknown"
	date    = "unknown"
)

func main() {
	build := engine.BuildInfo{Version: version, Commit: commit, Date: date}
	e := engine.New(name, description, build)

	e.RegisterRuleset(verwerkingenlogging.New())

	e.Run()
}
