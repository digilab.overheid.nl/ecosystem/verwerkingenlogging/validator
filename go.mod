module gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/validator

go 1.21.3

require (
	github.com/stretchr/testify v1.8.4
	gitlab.com/commonground/don/validators/engine v0.0.0-20231122140903-fddd4653d303
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/cobra v1.8.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
