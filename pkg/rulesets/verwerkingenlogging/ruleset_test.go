package verwerkingenlogging_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/validator/pkg/rulesets/verwerkingenlogging"
)

func TestRuleset(t *testing.T) {
	expected := []string{"OpenAPI"}
	actual := []string{}

	rs := verwerkingenlogging.New()

	for _, rule := range rs.Rules {
		actual = append(actual, rule.ID)
	}

	assert.Equal(t, expected, actual)
}
