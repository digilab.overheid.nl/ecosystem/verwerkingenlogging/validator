package verwerkingenlogging

import (
  "fmt"
  "io/ioutil"
	"net/http"
	"net/url"

  "gitlab.com/commonground/don/validators/engine/ruleset"
)

type testContext struct {
	client     *http.Client
	baseURL    *url.URL
}

func newTestContext(c *http.Client, u *url.URL) *testContext {
	return &testContext{
		client:  c,
		baseURL: u,
	}
}

func (ctx *testContext) Test(rule *ruleset.Rule) ruleset.Result {
	return rule.Test.(func(*testContext) ruleset.Result)(ctx)
}

func openapiRule(ctx *testContext) (r ruleset.Result) {
  specUrl := ctx.baseURL.JoinPath("openapi.json").String()

	resp, err := ctx.client.Get(specUrl)
	if err != nil {
	  r.Message = fmt.Sprintf("Could not fetch %s", specUrl)
	  r.Details = err.Error()

	  return
	}
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	if resp.StatusCode != 200 {
	  r.Message = fmt.Sprintf("Path %s does not exist", specUrl)
	  r.Details = r.Message

	  return
	}

	r.Passed = true

	return
}
