package verwerkingenlogging

import (
  "gitlab.com/commonground/don/validators/engine/ruleset"
)

const documentationURL = "https://gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/poc-verwerkingenlog/-/blob/main/README.md"

func New() *ruleset.Ruleset {
	return &ruleset.Ruleset{
		ID:    "verwerkingenlogging",
		Title: "Verwerkingenlogging Standard",
		Description: "This validator confirms wether or not a Verwerkingenlogging implemenation adheres to the Verwerkingenlogging Standard",
		Version:          "1.0.0",
		DocumentationURL: documentationURL,
		Rules: []*ruleset.Rule{
			ruleset.NewRule(
        "OpenAPI",
        "An OpenAPI spec is available",
        "An OpenAPI spec is available in json format",
				documentationURL,
				openapiRule,
			),
		},
		RuleTester: func(ctx *ruleset.Context) (ruleset.RuleTester, error) {
			testCtx := newTestContext(ctx.Client, ctx.BaseURL)

			return testCtx, nil
		},
	}
}
