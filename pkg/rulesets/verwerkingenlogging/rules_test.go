package verwerkingenlogging

import (
  "net/http"
	"net/http/httptest"
	"net/url"

	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOpenAPIRule(t *testing.T) {
	mux := http.NewServeMux()

	ts := httptest.NewServer(mux)
	u, _ := url.Parse(ts.URL)
	defer ts.Close()

	c := ts.Client()

	ctx := newTestContext(c, u)

	r := openapiRule(ctx)
	assert.False(t, r.Passed)

	mux.HandleFunc("/openapi.json", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	r = openapiRule(ctx)
	assert.True(t, r.Passed)
}
