# Verwerkingenlogging Validator

This validator can be used to ensure that an implementation of a Log component adheres to the standard of Verwerkingenlogging.

VWL validator works with the reference implemenation of Log (see below: Run) and should work with any other implementation to make sure the Log is compliant with the standard.

For usage you can run `vwl-validator --help`.

## Usage

### Build
```bash
go build -o vwl-validator ./cmd/vwl-validator
```

### Run
First ensure you have a Verwerkingenlogging "Log" implementation running, for instance the [reference implementation](https://gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/poc-verwerkingenlog), with a clean and disposable database (no fixtures, no entries).
```bash
./vwl-validator validate verwerkingenlogging http://localhost:8000/api/v0
```

## License
Licensed under EUPL v1.2
